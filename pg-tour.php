<?php
/*
Template Name: Course Tour Page
*/
?>

<?php get_header(); ?>

<?php if (get_field('banner_image')): ?>
  <div class="Strip TitleBanner" style="background-image:url(  <?php echo get_field('banner_image'); ?>)">
<?php else: ?>
  <div class="Strip TitleBanner" style="background-image:url( <?php bloginfo('template_url') ?>/assets/img/banner_main.jpg )">
<?php endif; ?>

  <div class="SectionContainer" style="position:relative; height:100%;">
    <h1 class="MainTitle u-verticalCenterTransform"><?php the_title(); ?></h1>
  </div>
</div>

<div class="Strip">
  <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <section class="EntryContent  cf">
        <?php the_content(); ?>
      </section> <!-- /EntryContent -->
    <?php endwhile; endif; // END main loop (if/while) ?>



    <div class="HoleSlider">

      <?php
      $args = array(
        'posts_per_page' => -1,
        'post_type' => 'hole_type',
        'orderby' => 'menu',
        'order' => 'asc'
      );
      $cpt_query = new WP_Query($args);
      ?>
      <?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>

        <div class="Hole">
          <div class="Hole-flexwrapper">
            <div class="Hole-details">
              <h4><span><?php echo get_field('hole_number'); ?></span><br />PAR <?php echo get_field('par'); ?></h4>

            </div>

            <div class="Hole-imageDescription">
              <?php the_post_thumbnail('large'); ?>
            </div>
            <div class="Hole-yardage">
              <h4>Yardage</h4>
              <ul>
                <li class="Hole-yardageMarker Hole-yardageMarker--black"><i class="fa fa-flag" aria-hidden="true"></i>Black <span><?php echo get_field('black_yardage'); ?></span></li>
                <li class="Hole-yardageMarker Hole-yardageMarker--blue"><i class="fa fa-flag" aria-hidden="true"></i>Blue <span><?php echo get_field('blue_yardage'); ?></span></li>
                <li class="Hole-yardageMarker Hole-yardageMarker--white"><i class="fa fa-flag" aria-hidden="true"></i>White <span><?php echo get_field('white_yardage'); ?></span></li>
                <li class="Hole-yardageMarker Hole-yardageMarker--red"><i class="fa fa-flag" aria-hidden="true"></i>Red <span><?php echo get_field('red_yardage'); ?></span></li>
              </ul>
            </div>
          </div>

          <div class="Hole-description"><?php the_content(); ?></div>


        </div>

      <?php endwhile; endif; // end of CPT loop ?>
      <?php wp_reset_postdata(); ?>
    </div>


    <div class="HoleNav">
      <div>1</div>
      <div id='2'>2</div>
      <div>3</div>
      <div>4</div>
      <div>5</div>
      <div>6</div>
      <div>7</div>
      <div>8</div>
      <div>9</div>
      <div>10</div>
      <div>11</div>
      <div>12</div>
      <div>13</div>
      <div>14</div>
      <div>15</div>
      <div>16</div>
      <div>17</div>
      <div>18</div>
    </div>


    <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

  </main>
</div> <!-- /Strip-->

<?php get_footer(); ?>

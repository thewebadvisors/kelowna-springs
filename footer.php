    <?php get_template_part( 'parts/review-banner' ); ?>

<footer class="Strip u-responsivePadding PageFooter" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
  <div class="SectionContainer FooterColumns">

    <div class="FooterColumn">
      <div class="FooterAddress">
        <?php if ( is_active_sidebar( 'footer-one' ) ) : ?>
        <?php dynamic_sidebar( 'footer-one' ); ?>
      <?php endif; ?>
     </div>
    </div>

    <div class="FooterColumn">
      <h4>Useful links</h4>
        <?php fdt_nav_menu( 'footer-nav', 'SecondaryMenu' ) // Adjust using Menus in Wordpress Admin ?>
      <ul>
        <li><a  style="font-size:20px;" target="_blank" href="https://secure.west.prophetservices.com/KelownaMemberV3/Account/LogOn?IsRequseLogin=True#Hash"><i class="fa fa-star-o"></i></a></li>
      </ul>
    </div>

      <div class="FooterColumn">
        <?php if ( is_active_sidebar( 'footer-third' ) ) : ?>
        <?php dynamic_sidebar( 'footer-third' ); ?>
      <?php endif; ?>
      </div>

      <div class="FooterColumn">
        <h4>Stay Informed</h4>
        <?php gravity_form(1, false, false, false, '', true); ?>
      </div>




  </div> <!-- /container-->
  <div class="FooterLower">
  <span class="SiteCopyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</span>
  </div>
</footer> <!-- /footer -->

<div class="white-popup-block mfp-hide" id="HeaderContactForm">
  <h5>Contact Us</h5>
  <?php echo do_shortcode('[gravityform id="6" title="false" description="false" ajax="false" tabindex="200"]'); ?>
</div>

<!-- all js scripts are loaded in lib/fdt-enqueues.php -->
<?php wp_footer(); ?>

</div> <!-- for mmenu -->
</body>

</html> <!-- end page. what a ride! -->

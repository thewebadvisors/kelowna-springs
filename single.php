<?php get_header(); ?>

<?php if (get_field('banner_image')): ?>
  <div class="Strip TitleBanner" style="background-image:url(  <?php echo get_field('banner_image'); ?>)">
<?php else: ?>
  <div class="Strip TitleBanner" style="background-image:url( <?php bloginfo('template_url') ?>/assets/img/banner_main.jpg )">
<?php endif; ?>

  <div class="SectionContainer" style="position:relative; height:100%;">
      <h1 class="MainTitle u-verticalCenterTransform" itemprop="headline  u-verticalCenter"><?php the_title(); ?></h1>
  </div>
</div>

<div class="Strip">


  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


    <div class="PrimaryContent">

      <article <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">


      <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

        <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
          <div class="EntryMeta">
            <span><time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('M d, Y'); ?></time></span>
            <!-- <span>Categories: <?php the_category(', '); ?></span> -->
          </div> <!-- /EntryMeta -->
          <?php the_content(); ?>

        </section> <!-- /EntryContent -->

        <footer class="ArticleFooter">
          <div class="LinkButton">
            <a href="/blog">&laquo; All Posts</a>
          </div>
        </footer> <!-- /article footer -->

      </article> <!-- /article -->

    <?php endwhile; else : ?>

      <article class="PostNotFound">
        <header class="ArticleHeader">
          <h1><?php _e("Oops, Post Not Found!", "flexdev"); ?></h1>
        </header>
        <section class="EntryContent">
          <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
        </section>
        <footer class="ArticleFooter">
          <p><?php _e("This is the error message in the single.php template.", "flexdev"); ?></p>
        </footer>
      </article>

    <?php endif; ?>

  </div> <!-- /PrimaryContent -->

</main>
</div> <!-- /Strip-->

<?php get_footer(); ?>

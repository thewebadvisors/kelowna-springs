<?php
/*
Template Name: Home Page
*/
?>

<?php get_header(); ?>
<div id="HeroSlider" class="HeroSlider">
  <?php if( have_rows('slides') ): ?>
    <?php while( have_rows('slides') ): the_row(); ?>

      <div class="HeroSlider-item">
        <img class="HeroSlider-img" <?php fdt_responsive_image( get_sub_field( 'slide_image' ), 'full', '1900px' ); ?> />
        <div class="HeroSlider-contentWrap  SectionContainer">
          <div class="HeroSlider-content">
            <?php if( get_sub_field('slide_title') ) { echo '<h4>' . get_sub_field('slide_title') . '</h4>'; }?>
            <?php if( get_sub_field('slide_sub_text') ) {  echo get_sub_field('slide_sub_text') ; }?>
          <?php if (get_sub_field('slide_link')) { ?>

            <div class="LinkButton"><a href="<?php the_sub_field('slide_link'); ?>" class="slide-link"><?php the_sub_field('slide_link_text') ?></a></div>

          <?php } ?>

          </div> <!-- /HeroSlider-content -->
        </div> <!-- /HeroSlider-contentWrap -->
      </div> <!-- /HeroSlider-item -->

    <?php endwhile; ?>
  <?php endif; ?>
</div> <!-- /HeroSlider -->


<div class="Strip  BookBar">
  <div class="SectionContainer">
    <div class="">
      <h4><a href="/golf/book-online">Book a Tee Time<img src="<?php bloginfo('template_url') ?>/assets/img/White_Spring.png" alt="" /></a></h4>
    </div> <!-- /BookBar -->
  </div> <!-- /SectionContainer --> 
</div>

  <div class="Strip Strip--afterheader">
    <main class="SectionContainerFull" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <section class="EntryContent  cf">
          <h1 class="MainTitle"><?php the_title(); ?></h1>
          <?php the_content(); ?>
        </section> <!-- /EntryContent -->

      <?php endwhile; endif; // END main loop (if/while) ?>

      <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>

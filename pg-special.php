<?php
/*
Template Name: Specials Template
*/
?>

<?php get_header(); ?>



<?php if (get_field('banner_image')): ?>
  <div class="Strip TitleBanner" style="background-image:url(  <?php echo get_field('banner_image'); ?>)">
  <?php else: ?>
    <div class="Strip TitleBanner" style="background-image:url( <?php bloginfo('template_url') ?>/assets/img/banner_main.jpg )">
    <?php endif; ?>

    <div class="SectionContainer" style="position:relative; height:100%;">
      <h1 class="MainTitle u-verticalCenterTransform"><?php the_title(); ?></h1>
    </div>
  </div>

  <div class="Strip">
    <main class="SectionContainer  cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">


      <div class="SpecialsAside">
        <a class="SpecialLink  SpecialLink-limited" href="/specials/limited-time/"></a>
        <div class="DaysAvailible">

        </div>

        <a class="SpecialLink  SpecialLink-backnine" href="/specials/back-9"></a>
        <div class="DaysAvailible">
          <span>S M T W T F S</span>
        </div>

        <a class="SpecialLink  SpecialLink-golfdinner" href="/specials/9-and-dine"></a>
        <div class="DaysAvailible">
        <span>S M T W T F S</span>
      </div>



        <a class="SpecialLink  SpecialLink-vegas" href="/specials/vegas-fun-days/"></a>
        <div class="DaysAvailible">
          S <span>M T</span> W T F S
        </div>

        <a class="SpecialLink  SpecialLink-ladiesnight" href="/specials/ladies-night/"></a>
        <div class="DaysAvailible">
          S M T <span>W</span> T F S
        </div>

        <a class="SpecialLink  SpecialLink-mensnight" href="/specials/mens-night/"></a>
        <div class="DaysAvailible">
          S M T W <span>T</span> F S
        </div>

        <a class="SpecialLink  SpecialLink-double" href="/specials/double-twilight/"></a>
        <div class="DaysAvailible">
          S <span>M T W T</span> F S
        </div>

        <a class="SpecialLink  SpecialLink-Juniors" href="/specials/playgolf-juniors/"></a>
        <div class="DaysAvailible">
          <span>S</span> M T W T <span>F S</span>
        </div>

        <a class="SpecialLink  SpecialLink-footgolf" href="/specials/foot-golf/"></a>
        <div class="DaysAvailible">
            <span>S</span> M T W T F  <span>S</span>
        </div>



      </div><div class="SpecialsMain">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <section class="EntryContent  cf">
            <?php the_content(); ?>
          </section> <!-- /EntryContent -->
        <?php endwhile; endif; // END main loop (if/while) ?>
      </div>

    </main>
  </div> <!-- /Strip-->

  <?php get_footer(); ?>

<?php get_header(); ?>

<div class="Strip TitleBanner" style="background-image:url( <?php bloginfo('template_url') ?>/assets/img/banner_main.jpg )">

<div class="SectionContainer" style="position:relative; height:100%;">
    <?php
        the_archive_title( '<h1 class="MainTitle u-verticalCenterTransform">', '</h1>' );
        the_archive_description( '<div class="MainTitle u-verticalCenterTransform taxonomy-description">', '</div>' );
      ?>
</div>
</div>

<div class="Strip">

<main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

  <div class="PrimaryContent">



    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

      <article <?php post_class('cf BlogWrap'); ?> role="article">


        <div class="BlogWrap-image">
          <?php the_post_thumbnail('medium'); ?>
        </div>


        <div class="BlogWrap-content">

          <header class="ArticleHeader">
            <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
            <div class="EntryMeta">
              <span>Date: <time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('d.m.Y'); ?></time></span>
              <span><?php the_category(' / '); ?></span>
            </div> <!-- /EntryMeta -->
          </header> <!-- /ArticleHeader -->

          <section class="EntryContent  cf">
            <?php the_excerpt(); ?>
          </section> <!-- /EntryContent -->

          <footer class="ArticleFooter">
            <?php // something?  ?>
          </footer> <!-- /ArticleFooter -->

        </div>

      </article> <!-- /article -->

    <?php endwhile; ?>

    <nav class="PostNav">
      <ul class="cf">
        <li class="PostNav-prev"><?php next_posts_link(__('&laquo; Older Entries', 'flexdev')) ?></li>
        <li class="PostNav-next"><?php previous_posts_link(__('Newer Entries &raquo;', 'flexdev')) ?></li>
      </ul>
    </nav> <!-- /PostNav -->

  <?php else : ?>

    <article class="PostNotFound">
      <header class="ArticleHeader">
        <h2><?php _e("Oops, Post Not Found!", "flexdev"); ?></h2>
      </header>
      <section class="EntryContent">
        <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
      </section>
      <footer class="ArticleFooter">
        <p><?php _e("This is the error message in the archive.php template.", "flexdev"); ?></p>
      </footer>
    </article>

  <?php endif; ?>

</div> <!-- /PrimaryContent -->


</main>
</div> <!-- /Strip-->

<?php get_footer(); ?>

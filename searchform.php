<form role="search" method="get" id="searchform" class="SearchForm" action="<?php echo home_url( '/' ); ?>">
  <div>
    <label for="s" class="u-visuallyHidden">Search for:</label>
    <input type="search" id="s" name="s" value="" class="SearchInput" placeholder="Search..." />
    <button type="submit" id="searchsubmit" class="SearchSubmit">Search</button>
  </div>
</form>

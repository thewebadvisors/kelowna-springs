<?php
/*
Template Name: Event-internaluse only
*/
?>

<?php get_header(); ?>



  <div class="Strip TitleBanner" style="background-image:url( <?php bloginfo('template_url') ?>/assets/img/banner_main.jpg )">

  <div class="SectionContainer" style="position:relative; height:100%;">
      <h1 class="MainTitle u-verticalCenterTransform">Events</h1>
  </div>
</div>

  <div class="Strip Strip--afterheader">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <section class="EntryContent  cf">

          <?php the_content(); ?>
        </section> <!-- /EntryContent -->

      <?php endwhile; endif; // END main loop (if/while) ?>

      <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>

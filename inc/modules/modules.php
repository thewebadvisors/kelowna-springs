<?php

FLBuilder::register_module( 'MyModuleClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Tab 1', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'my-field-1'     => array(
                        'type'          => 'text',
                        'label'         => __( 'Text Field 1', 'fl-builder' ),
                    ),
                    'my-field-2'     => array(
                        'type'          => 'text',
                        'label'         => __( 'Text Field 2', 'fl-builder' ),
                    )
                )
            )
        )
    )
) );
 ?>

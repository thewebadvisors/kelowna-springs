<?php
/*
This file handles the admin area and functions.
You can use this file to make changes to the dashboard.
*/

/**
 * CONTENTS
 *
 * ADMIN STUFF ------ #admin
 * Dashboard Widgets......................hide widgets you don't want to see
 * Admin area tweaks......................remove admin menu items; hide admin bar
 * Hide ACF from admin menu...............(commented out by default)
 * Custom Login Page......................styles; logo URL
 *
 * VISUAL EDITOR ------ #editor
 * Editor-style.css.......................
 * Limit number of revisions..............
 * Visual Blocks TinyMCE plugin...........
 * Other TinyMCE edits....................
 *
 * OTHER STUFF ------ #other
 * Google fonts in text editor............enque fonts used in editor-styles (off by default)
 * Favicon in admin.......................enable if not using WP favicon (off by default)
 * Custom footer..........................promote yourself in the WP footer (off by default)
 *
 */


/*********************************************
ADMIN STUFF - #clean
*********************************************/

/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
  global $wp_meta_boxes;
  // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);    // Right Now Widget
  // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);    // Activity Widget
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);  // Comments Widget
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);  // Incoming Links Widget
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);      // Plugins Widget

  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);    // Quick Press Widget
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);    // Recent Drafts Widget
  // unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);      //
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);      //

  // remove plugin dashboard boxes
  unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);      // Yoast's SEO Plugin Widget
  unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);      // Gravity Forms Plugin Widget
  unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);  // bbPress Plugin Widget
  unset($wp_meta_boxes['dashboard']['normal']['core']['tribe_dashboard_widget']);  // TEC Plugin Widget
}

// removing the dashboard widgets
add_action( 'wp_dashboard_setup', 'disable_default_dashboard_widgets' );


/************* CUSTOMIZE ADMIN *******************/

// remove unnecessary admin menus
function hide_menu_items() {
  if( current_user_can( 'update_core' ) ):
    remove_menu_page( 'edit-comments.php' ); // Comments
  endif;
  if( !current_user_can( 'update_core' ) ):
    remove_menu_page( 'index.php' ); // Dashboard tab
    remove_menu_page( 'edit.php?post_type=page '); // Pages
    remove_menu_page( 'upload.php' ); // Media
    remove_menu_page( 'link-manager.php' ); // Links
    remove_menu_page( 'edit-comments.php' ); // Comments
    remove_menu_page( 'themes.php' ); // Appearance
    remove_menu_page( 'plugins.php' ); // Plugins
    remove_menu_page( 'users.php' ); // Users
    remove_menu_page( 'tools.php' ); // Tools
    remove_menu_page( 'options-general.php' ); // Settings
    // remove_menu_page( 'edit.php?post_type=slider_type' );
    // remove_menu_page( 'acf-options' );
  endif;
  }
add_action( 'admin_menu', 'hide_menu_items', 999 );


// Disable the Wordpress Admin Bar for all but admins.
if (!current_user_can('update_core')):
  show_admin_bar(false);
endif;


// REMOVE ACF FROM ADMIN MENU
// add_filter('acf/settings/show_admin', '__return_false');



/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it
// updated to proper 'enqueue' method
// http://codex.wordpress.org/Plugin_API/Action_Reference/login_enqueue_scripts
function fdt_login_css() {
  wp_enqueue_style( 'fdt_login_css', get_template_directory_uri() . '/assets/css/login.css', false );
}

// changing the logo link from wordpress.org to your site
function fdt_login_url() { return home_url(); }

// changing the alt text on the logo to show your site name
function fdt_login_title() { return get_option('blogname'); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'fdt_login_css', 10 );
add_filter( 'login_headerurl', 'fdt_login_url' );
add_filter( 'login_headertitle', 'fdt_login_title' );




/*********************************************
VISUAL EDITOR - #editor
*********************************************/

/************* TINYMCE CUSTOMIZATIONS *****************/

// This bring in cautom styles to your visual editor with editor-style.css
add_editor_style();


// Limit Number of Post & Page Revisions
add_filter( 'wp_revisions_to_keep', 'limit_revisions_by_posttype', 10, 2 );
function limit_revisions_by_posttype( $num, $post ) {
  $num = 15;
  return $num;
}


// TinyMCE edits: add visualblock plugin
add_filter('mce_external_plugins', 'ub_add_mceplugins');
function ub_add_mceplugins($plugins) {
  $plugins[ 'visualblocks' ] = get_stylesheet_directory_uri() . '/assets/js/visualblocks/visualblocks_plugin.js';
  return $plugins;
}
function myformatTinyMCE($in) {
  $in['toolbar2'].=',visualblocks ';
  $in['visualblocks_default_state'] = true;
  return $in;
}
add_filter('tiny_mce_before_init', 'myformatTinyMCE' );


// Kitchen sink (toolbar2) has good stuff, let's show it by default
function unhide_kitchensink( $args ) {
  $args['wordpress_adv_hidden'] = false;
  return $args;
}
add_filter( 'tiny_mce_before_init', 'unhide_kitchensink' );




/*********************************************
OTHER STUFF - #other
*********************************************/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'flexdev_flush_rewrite_rules' );
function flexdev_flush_rewrite_rules() {
  flush_rewrite_rules();
}


/************* GOOGLE FONTS ***********************/
// function fdt_fonts() {
//   wp_enqueue_style('googleFonts', 'http://fonts.googleapis.com/css?family=Merriweather:400,700,900,400italic,700italic,900italic');
// }



/************* FAVICON FOR WP ADMIN URL (if not using WP favicon setup) *****************/
// // First, create a function that includes the path to your favicon
// function add_favicon() {
//   $favicon_url = '//favicon-32x32.png';
//   echo '<link rel="icon" type="image/png" href="' . $favicon_url . '" sizes="32x32" />';
// }
// // Now, just make sure that function runs when you're on the login page and admin pages
// add_action('login_head', 'add_favicon');
// add_action('admin_head', 'add_favicon');




/************* CUSTOM BACKEND FOOTER **************/
// function ub_custom_admin_footer() {
//   echo '<span id="footer-thankyou">Developed by <a href="http://yoursite.com" target="_blank">Company Name</a></span>.';
// }
//
// // adding it to the admin area
// add_filter('admin_footer_text', 'ub_custom_admin_footer');



?>

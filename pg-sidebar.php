<?php
/*
Template Name: Right Sidebar
*/
?>

<?php get_header(); ?>

<?php if (get_field('banner_image')): ?>
  <div class="Strip TitleBanner" style="background-image:url(  <?php echo get_field('banner_image'); ?>)">
<?php else: ?>
  <div class="Strip TitleBanner" style="background-image:url( <?php bloginfo('template_url') ?>/assets/img/banner_main.jpg )">
<?php endif; ?>

  <div class="SectionContainer" style="position:relative; height:100%;">
      <h1 class="MainTitle u-verticalCenterTransform"><?php the_title(); ?></h1>
  </div>
</div>

  <div class="Strip">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <section class="EntryContent EntryContentWithSidebar   cf">
          <?php the_content(); ?>
        </section> <!-- /EntryContent -->
      <?php endwhile; endif; // END main loop (if/while) ?>

        <?php get_sidebar(); // sidebar ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>

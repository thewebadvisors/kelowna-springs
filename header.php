<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <?php // force Internet Explorer to use the latest rendering engine available ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php // mobile meta (hooray!) ?>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <?php // icons & favicons ?>
  <?php // Using REAL FAVICON GENERATOR? Put generated code in file below + uncomment admin favicon setup (in admin.php) ?>
  <?php // get_template_part( 'favicon-head' ); ?>

  <?php // other html head stuff (before WP/theme scripts are loaded) ?>
  <script src="https://use.fontawesome.com/ad04f5a461.js"></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700|Raleway:400,400i,600,700" rel="stylesheet">

  <!-- wordpress head functions -->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->

  <?php // drop Google Analytics here ?>
  <?php // end Analytics ?>

</head>

<body <?php body_class(pretty_body_class()); ?> itemscope itemtype="http://schema.org/WebPage">
  <!--[if lt IE 11]><p class="browserwarning">Your browser is <strong>very old.</strong> Please <a href="http://browsehappy.com/">upgrade to a different browser</a> to properly browse this web site.</p><![endif]-->


  <!-- for mmenu --><div>

  <div class="Strip  Strip--nopad  Fixed  TopHeader">
    <div class="SectionContainer">
      <div class="HeaderUpper">
        <span>250-765-4563</span>|
        <a class="MailLink  popup-with-form" href="#HeaderContactForm"> <i class="fa fa-envelope"></i></a>


        <div class="HeaderCTAs"><a class="hs-wifi" href="/gps">GPS APP</a><a href="/golf/book-online">Tee Times <i class="fa fa-clock-o" aria-hidden="true"></i></a></div>
        <div class="HeaderSocial">
          <!-- all on one line for inline-block spacing issue -->
          <a class="hs-facebook" target="_blank" href="http://www.facebook.com/pages/Kelowna-Springs-Golf-Club/156283067736556"><i title="facebook" class="fa fa-facebook" aria-hidden="true"></i><span class="u-hidden">Facebook</span></a><a target="_blank" class="hs-tripadvisor" href="https://www.tripadvisor.ca/Attraction_Review-g154933-d1994541-Reviews-Kelowna_Springs_Golf_Club-Kelowna_Okanagan_Valley_British_Columbia.html"><i title="hs-tripadvisor" class="fa fa-tripadvisor" aria-hidden="true"></i><span class="u-hidden">Tripadvisor</span></a><a class="hs-google" target="_blank" href="https://www.google.ca/maps/place/Kelowna+Springs+Golf+Club/@49.9219276,-119.3789558,15z/data=!4m5!3m4!1s0x0:0xf30ef595c8767fe6!8m2!3d49.9219276!4d-119.3789558"><i title="Goolge" class="fa fa-google" aria-hidden="true"></i><span class="u-hidden">Google Plus</span></a>
        </div>

      </div>
    </div>
  </div>

  <header class="Strip  Strip--nopad  PageHeader  Fixed" role="banner" itemscope itemtype="http://schema.org/WPHeader">

    <div class="SectionContainer">

      <div class="SiteLogo" itemscope itemtype="http://schema.org/Organization">
        <a href="/" rel="nofollow">
          <img src="<?php bloginfo('template_url') ?>/assets/img/logo.jpg" alt="<?php bloginfo('name'); ?>" />
        </a>
      </div>

      <nav class="MainMenuWrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <ul class="MainMenu">

          <?php
          $pageargs=array(
            'depth' => 2,
            'title_li' => '',
            'sort_column' => 'menu_order',
            'sort_order' => 'asc',
            'include_tree' => '6,9,101,110,26,10'
          );
          ub_wp_list_pages($pageargs);
          ?>

          <!-- <li class="MenuLinkButton"><a href="/blog">Blog</a></li> -->
        </ul>
        <?php // fdt_nav_menu( 'main-nav', 'MainMenu' ); // Adjust using Menus in WordPress Admin ?>
      </nav>

      <nav class="TertiaryMenu" role="navigation">
        <?php fdt_nav_menu( 'tertiary-nav', 'TertiaryMenu' ); // Adjust using Menus in WordPress Admin -- REMOVE if not using ?>
      </nav>

      <div class="ScrollMenu">
        <a class="ScrollMenu-call" href="tel:2507654653">Call us</a><a class="ScrollMenu-book" href="/golf/book-online">Book Now</a><a href="#nav-menu" class="ScrollMenu-mmenu">OPEN MENU</a><a class="ScrollMenu-top  ScrollTopMenu" id="MenuPopoutButton" href="#">Menu</a>

      </div>

      <nav role="navigation" id="nav-menu" class="nav-menu">
        <ul>
          <?php
          $pageargs=array(
            'depth' => 2,
            'title_li' => '',
            'sort_column' => 'menu_order',
            'sort_order' => 'asc',
            'include_tree' => '6,9,101,110,26,10'
          );
          ub_wp_list_pages($pageargs);
          ?>
        </ul>
      </nav>

    </div> <!-- /SectionContainer -->

  </header> <!-- /PageHeader -->

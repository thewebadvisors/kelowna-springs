<?php get_header(); ?>

  <div class="Strip">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <h1 class="archive-title"><span>Search Results for:</span> <?php echo esc_attr(get_search_query()); ?></h1>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article <?php post_class('cf'); ?> role="article">
          <header class="ArticleHeader">
            <h3 class="search-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
            <p class="EntryMeta  Byline">
              Posted: <time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('F jS, Y'); ?></time> |
              <span class="EntryAuthor" itemprop="author" itemscope itemptype="http://schema.org/Person">By: <?php the_author_posts_link(); ?> </span>
              | Topic: <?php the_category(', '); ?>.
            </p>
          </header> <!-- /ArticleHeader -->

          <section class="EntryContent">
            <?php the_excerpt(); ?>
          </section> <!-- /EntryContent -->

        </article> <!-- /article -->

      <?php endwhile; ?>

        <nav class="PostNav">
          <ul class="cf">
            <li class="PostNav-prev"><?php next_posts_link(__('&laquo; Older Entries', "flexdev")) ?></li>
            <li class="PostNav-next"><?php previous_posts_link(__('Newer Entries &raquo;', "flexdev")) ?></li>
          </ul>
        </nav>

      <?php else : ?>

        <article class="PostNotFound">
          <header class="ArticleHeader">
            <h4><?php _e("Sorry, No Results.", "flexdev"); ?></h4>
          </header>
          <section class="EntryContent">
            <p><?php _e("Please try another search.", "flexdev"); ?></p>
          </section>
        </article>

      <?php endif; ?>



    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>

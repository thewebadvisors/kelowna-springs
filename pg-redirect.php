<?php
/*
Template Name: Redirect Page
*/
$redirectOption = esc_html( get_field('redirect_to_where') );
if ($redirectOption == "internal") {
  wp_redirect( esc_html( get_field('internal_redirect'), 301 ) );
} elseif ($redirectOption == "external") {
  wp_redirect( esc_html( get_field('external_redirect'), 301 ) );
} else {
  $pagekids = get_pages("child_of=".$post->ID."&sort_column=menu_order");
  $firstchild = $pagekids[0];
  wp_redirect(get_permalink($firstchild->ID), 301);
}
?>

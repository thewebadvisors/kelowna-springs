<?php
/*
Template Name: Landing Page
*/
?>

<?php get_header('landing'); ?>

  <div class="Strip">
    <main class="SectionContainerFull" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <section class="EntryContent  cf">

          <?php the_content(); ?>
        </section> <!-- /EntryContent -->

      <?php endwhile; endif; // END main loop (if/while) ?>

      <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer('landing'); ?>

<?php
/*
Template Name: Full width Page with Banner
*/
?>

<?php get_header(); ?>


<?php if (get_field('banner_image')): ?>
  <div class="Strip TitleBanner" style="background-image:url(  <?php echo get_field('banner_image'); ?>)">
<?php else: ?>
  <div class="Strip TitleBanner" style="background-image:url( <?php bloginfo('template_url') ?>/assets/img/banner_main.jpg )">
<?php endif; ?>
  <div class="SectionContainer" style="position:relative; height:100%;">
      <h1 class="MainTitle u-verticalCenterTransform"><?php the_title(); ?></h1>
  </div>
</div>

  <div class="Strip Strip--afterheader">
    <main class="SectionContainerFull" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <section class="EntryContent  cf">

          <?php the_content(); ?>
        </section> <!-- /EntryContent -->

      <?php endwhile; endif; // END main loop (if/while) ?>

      <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>

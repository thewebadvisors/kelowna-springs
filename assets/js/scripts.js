/**
 * FlexDev Theme Scripts File **************
 * This file contains any js scripts you want added to the theme. Instead of calling it in the header or throwing
 * it inside wp_head() this file will be called automatically in the footer so as not to slow the page load.
 */



/* JQUERY VIEWPORT RE-SIZING? --> SEE HELPERS/RESPONSIVE MISC */


jQuery(document).ready(function ($) {
// *********************** START CUSTOM SCRIPTS *******************************


//Mobile nav
   $("#nav-menu").mmenu({
     offCanvas: {
       position: "right"
     },
     extensions: ["theme-dark"]
     },
     {
     classNames: {
       selected: "current_page_item"
     }
   });

  // init magnificPopup setup for images *****************
  // $('.magnific').magnificPopup({
  //   type: 'image',
  //   closeOnContentClick: true,
  //   closeBtnInside: false,
  //   fixedBgPos: true,
  //   mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
  //   image: {
  //     verticalFit: true
  //   },
  //   zoom: {
  //     enabled: true,
  //     duration: 300 // don't foget to change the duration also in CSS; ZOOM DOES NOT WORK ON TEXT LINK
  //   }
  // });

  $('.HoleSlider').slick({
     asNavFor: '.HoleNav',
     fade: true,
     speed: 800,
     swipe: true,
     touchMove: false,
     draggable: true,
     arrows: false,
     adaptiveHeight: true,
     waitForAnimate: false
    });

   $('.HoleNav').slick({
     asNavFor: '.HoleSlider',
     slidesToShow: 18,
     arrows: false,
      waitForAnimate: false,
     focusOnSelect: true,
     responsive: [
      {
        breakpoint: 1135,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 3,
          centerMode: true,
          arrows: true,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
          centerMode: true,
          arrows: true,
        }
      },
      {
        breakpoint: 530,
        settings: {
            slidesToShow: 3,
            centerMode: true,
            arrows: true,
        }
      }
    ]
   });


   $('.ReviewSlider').slick({
   fade: true,
   speed: 800,
   swipe: true,
   touchMove: false,
   slidesToShow: 1,
   draggable: true,
   arrows: false,
   adaptiveHeight: true,
   dots: true,
   waitForAnimate: false
  });



// MAGNIFIC POPUPS
  $('.popup-with-form').magnificPopup({
  		type: 'inline',
  		preloader: false,
  		focus: '#input_1',

  		// When elemened is focused, some mobile browsers in some cases zoom in
  		// It looks not nice, so we disable it:
  		callbacks: {
  			beforeOpen: function() {
  				if($(window).width() < 700) {
  					this.st.focus = false;
  				} else {
  					this.st.focus = '#input_1';
  				}
  			}
  		}
  	});


  // objectfit polyfill
  $(function () { objectFitImages('.u-polyFit'); });


//Scroll Menu
  $(window).scroll(function() {
  if ($(this).scrollTop() > 1){
      $('.PageHeader').addClass("sticky");
    }
    else{
      $('.PageHeader').removeClass("sticky");
    }
  });

  $(window).scroll(function() {
if ($(this).scrollTop() > 1){
    $('.Strip--afterheader').addClass("LessMargin");
  }
  else{
    $('.Strip--afterheader').removeClass("LessMargin");
  }
});

$( "#MenuPopoutButton" ).click(function() {
  $("html, body").animate({ scrollTop: 0 }, 300);
    return false;
});

// hero slider - slick carousel init
$('#HeroSlider').slick({
  arrows: false,
  dots: false,
  infinite: true,
  pauseOnHover: false,
  pauseOnFocus: false,
  // appendDots: '.HeroSlider-item', 
  speed: 500,
  fade: true,
  cssEase: 'linear',
  autoplay: true,
  autoplaySpeed: 4000
});


// *********************** END CUSTOM SCRIPTS *********************************

}); /* end of DOM ready scripts */

<?php if (is_page('110')): ?>

  <div class="Strip BlueBanner ">
  <div class="SectionContainer u-relative">
    <div class="Reviews  u-verticalCenterTransform">
      <?php
        $args = array(
          'posts_per_page' => 5,
          'post_type' => 'review_type',
          'orderby' => 'rand',
          'tax_query' => array(
            array(
              'taxonomy' => 'reviewcat_tax',
              'terms' => 'weddings',
               'field' => 'slug',
            )
          )
          // 'order' => 'asc'
        );
        $cpt_query = new WP_Query($args);
      ?>
        <div class="ReviewSlider">
      <?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>


          <div class="ReviewSlider-item">
            <p><?php echo get_field('review_content'); ?><br /><span><?php echo get_field('review_author'); ?></span></p>
          </div>


      <?php endwhile; endif; // end of CPT loop ?>
        </div> <!-- /ReviewSlider -->
      <?php wp_reset_postdata(); ?>

    </div> <!-- /Reviews -->
  </div> <!-- /SectionContainer -->
</div>

<?php else: ?>




<div class="Strip BlueBanner ">
  <div class="SectionContainer u-relative">
    <div class="Reviews  u-verticalCenterTransform">
      <?php
        $args = array(
          'posts_per_page' => 5,
          'post_type' => 'review_type',
          'orderby' => 'rand',
          'tax_query' => array(
            array(
              'taxonomy' => 'reviewcat_tax',
              'terms' => array('weddings'),
               'field' => 'slug',
               'operator' => 'NOT IN',
            )
          )
          // 'order' => 'asc'
        );
        $cpt_query = new WP_Query($args);
      ?>
        <div class="ReviewSlider">
      <?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>


          <div class="ReviewSlider-item">
            <p><?php echo get_field('review_content'); ?><br /><span><?php echo get_field('review_author'); ?></span></p>
          </div>


      <?php endwhile; endif; // end of CPT loop ?>
        </div> <!-- /ReviewSlider -->
      <?php wp_reset_postdata(); ?>

    </div> <!-- /Reviews -->
  </div> <!-- /SectionContainer -->
</div>
<?php endif; ?>
